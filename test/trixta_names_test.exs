defmodule TrixtaNamesTest do
  use ExUnit.Case
  # doctest TrixtaNames

  test "generates trixta name" do
    assert TrixtaNames.generate() =~ ~r/^[a-z]+-[a-z]+-[0-9]+/
  end
end
