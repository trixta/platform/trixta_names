defmodule TrixtaNames.Namer do
  @moduledoc """
  Module for accessing TrixtaNames services.
  """
  alias TrixtaNames.{Adjectives, Nouns}

  @doc """
  Generate a random URL friendly TrixtaNames

  ## Examples

      iex> TrixtaNames.Namer.generate()
      "polished-forest-7935"

  """
  def generate() do
    [
      Adjectives.random_adjective(),
      Nouns.random_noun(),
      :rand.uniform(9999)
    ]
    |> Enum.join("-")
  end
end
