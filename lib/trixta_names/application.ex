defmodule TrixtaNames.Application do
  @moduledoc """
  Application that manages TrixtaNames services.
  """
  use Application

  @doc """
  Start and supervise the TrixtaNames service processes.
  """
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      TrixtaNames.Adjectives,
      TrixtaNames.Nouns
    ]

    options = [
      name: TrixtaNames.Supervisor,
      strategy: :one_for_one
    ]

    Supervisor.start_link(children, options)
  end
end
