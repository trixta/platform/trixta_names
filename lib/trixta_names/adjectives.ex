defmodule TrixtaNames.Adjectives do
  @moduledoc """
  Service that manages adjectives list and retrieving random adjective.
  """

  use Agent

  @me __MODULE__

  @doc """
  Start the adjective list service process.
  """
  def start_link(_state) do
    Agent.start_link(&adjective_list/0, name: @me)
  end

  @doc """
  Retrieves a random adjective
  """
  def random_adjective() do
    adjective = Agent.get(@me, &Enum.random/1)
    if adjective == "", do: random_adjective(), else: adjective
  end

  defp adjective_list do
    Path.join(:code.priv_dir(:trixta_names), "adjectives.txt")
    |> File.read!()
    |> String.split(~r/\n/)
  end
end
