defmodule TrixtaNames.Nouns do
  @moduledoc """
  Service that manages nouns list and retrieving random noun.
  """

  use Agent

  @me __MODULE__

  @doc """
  Start the noun list service process.
  """
  def start_link(_state) do
    Agent.start_link(&noun_list/0, name: @me)
  end

  @doc """
  Retrieves a random noun
  """
  def random_noun() do
    noun = Agent.get(@me, &Enum.random/1)
    if noun == "", do: random_noun(), else: noun
  end

  defp noun_list do
    Path.join(:code.priv_dir(:trixta_names), "nouns.txt")
    |> File.read!()
    |> String.split(~r/\n/)
  end
end
