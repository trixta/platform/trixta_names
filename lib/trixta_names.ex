defmodule TrixtaNames do
  @moduledoc """
  TrixtaNames API.
  """
  alias TrixtaNames.Namer

  @doc """
  Generate a random URL friendly TrixtaNames

  ## Examples

      iex> TrixtaNames.generate()
      "polished-forest-7935"

  """
  defdelegate generate(), to: Namer
end
